import { Injectable, NotFoundException } from '@nestjs/common';
import { Todo } from './interfaces/todo.interface';
import { CreateTodoDto } from 'src/dto/create-todo.dto';

@Injectable()
export class TodosService {
  todos: Todo[] = [
    {
      id: 2,
      title: 'code',
      description: 'bla bla bla',
      done: false,
    },
    {
      id: 1,
      title: 'code code',
      description: 'bla bla bla bla blaaa',
      done: true,
    },
  ];

  findOne(id: string) {
    return this.todos.find((todo) => todo.id === Number(id));
  }

  findAll(): Todo[] {
    return this.todos;
  }

  create(todo: CreateTodoDto) {
    this.todos = [...this.todos, todo]; // recup. la liste de todos & ajoute le todo qui a été passé ds cette méthode "create"
  }

  update(id: string, todo: Todo) {
    const todoToUpdate = this.todos.find((t) => t.id === +id);
    if (!todoToUpdate) {
      return new NotFoundException('');
    }

    if (todo.hasOwnProperty('done')) {
      todoToUpdate.done = todo.done;
    }

    if (todo.title) {
      todoToUpdate.title = todo.title;
    }

    if (todo.description) {
      todoToUpdate.description = todo.description;
    }

    /**
     * test si le todo qu'on est en train d'itéré est différend de l'id qui ns a été passé
     * on recoie cet id en 1er parametre de la fnc update
     * si c'est pas le même id, ok, on le garde ds le new array qui va etre générer par map, pr k le todo en question soit concervé, sinon on remplace par le todo kon a mis a jour
     */
    const updatedTodos = this.todos.map((t) =>
      t.id !== +id ? t : todoToUpdate,
    );
    this.todos = [...updatedTodos];
    return { updatedTodos: 1, todo: updatedTodos };
  }
}
