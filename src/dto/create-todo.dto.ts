/* eslint-disable prettier/prettier */
// fr: classe décrit l objet transferé par 1 requête de type Post
// en: describe a Data Transfer Object, an Object that is transfered by a Post request
export class CreateTodoDto {
  readonly id: number;
  readonly title: string;
  readonly description?: string;
  readonly done: boolean;
}
