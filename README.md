<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="60" alt="Nest Logo" /></a>
</p>

# Todo `NestJS` application

## @Decorateurs TS dans NestJS

- `@Param` pr préciser un paramêtre
- verbes de l'API `HTTP`:
  - `@Get` pr lire, recevoir ...
  - `@Post` pr envoyer, créer...
  - `@Patch` pr mettre à jour, update ...
- `@Body` pr récupérer le body d'une requète (utilisé sur les Post & Patch notament...)

## Création d'un `Module`, d'un `Controlleur` & d'un `Service`

### Création du `Module`

- utilisons la forme courte avec la `cli` de `nest`
  - `g` correspond à `generate`
  - `mo` à `module`

```bash
$ nest g mo todos
```

### Création du `Controlleur`

- Rappel: il est de la responsabilité du controlleur d'écouter les requêtes entrantes et de fournir une réponse sortante
- Pr créer un controlleur avec la `cli`:

```bash
$ nest g co todos
```

- On remarque que le controlleur est associé au module créé précédament.
- & on importe maintenant le controlleur ds le module...

* on veut écouter une requête vers l'url précisé par le décorateur `@Controller('todos')` (qui revoie vers l'url `localhost:3000/todos`)

- pour pouvoir écouter les requêtes de type `Get`, il faut importer `Get` depuis `'@nestjs/common'`
- on recupère nos `todos` avec un méthode que ns allons nommer `findAll`, qui retourne une promesse (`Promise`), d'un tableau à typer.

- ns allons avoir besoin de générer un service charger de nous fournir ce qu'on va retourner ds notre néthode.

### Création du `Service`

```bash
$ nest g s todos
```

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Kamil Myśliwiec](https://kamilmysliwiec.com)
- Website - [https://nestjs.com](https://nestjs.com/)
- Twitter - [@nestframework](https://twitter.com/nestframework)

## License

Nest is [MIT licensed](LICENSE).
